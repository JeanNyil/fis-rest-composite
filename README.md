# REST composite service tutorial (using JBoss Developer Studio)

This tutorial will guide you on how to build a composite service exposed as REST that will compose a response based on the data collected from two different SOAP backend services (also included). It will also show you how to deploy the full solution in OpenShift.

The REST service is implemented using Camel's REST DSL, and all the SOAP interactions at the back are implemented using Camel's CXF component. All components are based on SpringBoot.

The picture below shows all the components the tutorial will help you to build step by step:

![](images/fullview.png)

The instructions are provided in the format of 2 PDF files.
- [1_instructions-soap-service.pdf](./1_instructions-soap-service.pdf)
- [2_instructions-rest-service.pdf](./2_instructions-rest-service.pdf)

Chapter 1 will guide you on how to build the backend SOAP services.

Chapter 2 will guide you on how to build the frontend REST composite service.

